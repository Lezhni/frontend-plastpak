var hwSlideSpeed = 700;
var hwTimeOut = 3000;
var hwNeedLinks = true;
var dh = 'left';
var ds = 'right';
 
$(document).ready(function(e) {
    $('.slide').css(
        {"position" : "absolute",
         "top":'0', "left": '0'}).hide().eq(0).show();
    var slideNum = 0;
    var slideTime;
    slideCount = $("#slider .slide").size();
    var animSlide = function(arrow){
        clearTimeout(slideTime);
        if(arrow == "next") {
            dh = 'left';
            ds = 'right';

        } else if(arrow == "prew") {
            ds = 'left';
            dh = 'right';
        } else {
            if(slideNum < arrow) {
                dh = 'left';
                ds = 'right';
            } else {
                ds = 'left';
                dh = 'right';
            }
        }
        $('.slide').eq(slideNum).hide('slide',{direction: dh},hwSlideSpeed);
        if(arrow == "next"){
            if(slideNum == (slideCount-1)){slideNum=0;}
            else{slideNum++}
                dh = 'left';
                ds = 'right';
            }
        else if(arrow == "prew")
        {
            if(slideNum == 0){slideNum=slideCount-1;}
            else{slideNum-=1}
                ds = 'left';
                dh = 'right';
        }
        else{
            slideNum = arrow;
            }
        $('.slide').eq(slideNum).show('slide', {direction: ds}, hwSlideSpeed, rotator);
        $(".control-slide.active").removeClass("active");
        $('.control-slide').eq(slideNum).addClass('active');
        }
if(hwNeedLinks){
var $linkArrow = $('<div id="slide-nav"><a id="prewbutton" href="#"></a><a id="nextbutton" href="#"></a></div>')
    .prependTo('#slider');      
    $('#nextbutton').click(function(){
        animSlide("next");
 
        })
    $('#prewbutton').click(function(){
        animSlide("prew");
        })
}
    var $adderSpan = '';
    $('.slide').each(function(index) {
            $adderSpan += '<span class = "control-slide">' + index + '</span>';
        });
    $('<div class ="sli-links">' + $adderSpan +'</div>').insertAfter('#prewbutton');
    $(".control-slide:first").addClass("active");
     
    $('.control-slide').click(function(){
    var goToNum = parseFloat($(this).text());
    animSlide(goToNum);
    });
    var pause = false;
    var rotator = function(){
    if(!pause){slideTime = setTimeout(function(){animSlide('next')}, hwTimeOut);}
            }
    $('#slider, .slide, .slide-block').hover(    
        function(){clearTimeout(slideTime); pause = true;},
        function(){pause = false; rotator();
        });
    rotator();
});